const csvToJson = {
    init: function (target, html) {
        let file = target.files[0];

        Papa.parse(file, {
            header: true,
            dynamicTyping: true,
            complete: function (results) {
                csvToJson.generatePapaTable(results, html);
            }
        })
    },
    generatePapaTable: function (item, html) {
        const title = item.meta.fields
        const content = item.data

        let header = title.map(th => `<th>${th}</th>`).join("")

        let tbody = content.map(td => {
            let row = Object.values(td).map((cell, index) => {
                return index == 0 ? `<th>${cell}</th>` : `<td>${cell}</td>`
            }).join("")

            return `<tr>${row}</tr>`
        }).join("")

        //version optimisé avec reduce
        // const header = title.reduce((acc, th) => acc + `<th>${th}</th>`, "");
        // const tbody = content.reduce((acc, td) => {
        //     const row = Object.values(td).reduce((r, cell) => r + `<td>${cell}</td>`, "");
        //     return acc + `<tr>${row}</tr>`;
        // }, "");

        //build a table
        document.querySelector(html).innerHTML = `
            <table id="dataTable" class="dataTable table">
                <thead>${header}</thead>
                <tbody>${tbody}</tbody>
            </table>`

        global.dataTables("dataTable")
    },
}