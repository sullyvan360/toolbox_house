const cacheName = "header-cache";

const global = {
    handleResult: function (results) {
        const errors = results
            .filter(result => result.status === "rejected")
            .map(result => result.reason)

        if (errors.length) {
            console.log(errors)
            throw new AggregateError(errors)
        }

        return results.map(result => result.value)
    },
    handleError: function (err) {
        console.error('Error while fetching header:', err);
        alert(err);
    },
    fetchHeader: async function () {
        const response = await fetch('partials/header.html')

        if ('caches' in window) {
            const cache = await caches.open(cacheName);
            await cache.put("partials/header.html", response.clone());
        }

        return response.text();
    },
    fetchGit: async function () {
        const response = await fetch('.git/HEAD')
        return response.text();
    },
    generateHeader: async function () {
        try {
            const results = await Promise.allSettled([
                global.fetchHeader(),
                global.fetchGit()
            ])

            const [header, git] = await global.handleResult(results);

            document.body.insertAdjacentHTML('afterbegin', header);

            anime({
                targets: ".header",
                translateY: [-100, 0],
                opacity: [0, 1],
                duration: 500,
                easing: 'easeOutExpo'
            });

            const branch = git.match(/ref: refs\/heads\/(.*)/)[1];
            const branchName = document.getElementById('currentBranchGit');
            branchName.innerText = branch;

            const children = [...document.getElementById('navigation').children];
            if (document.title === 'accueil') return children[0].classList.add('active');

            children.forEach(item => {
                if (document.title === item.dataset.navigation) {
                    item.classList.add('active');
                    item.setAttribute('aria-current', 'page');
                }
            });
        } catch (error) {
            global.handleError(error);
        }
    },
    checkSiretExist: async function (siret) {
        const restaurants = await directus.init("restaurants")
        const value = siret.value

        const restaurant_exist = restaurants.find(item => item.siret === value)

        if (restaurant_exist) {
            console.log(restaurant_exist)
        } else {
            const getEntreprise = await global.getEntrepriseBySiret(value)
            if (getEntreprise == "error") return siret.classList.add("is-invalid");

            siret.classList.remove("is-invalid")
            siret.classList.add("is-valid")

            const myEntreprise = {
                name: getEntreprise.uniteLegale.denominationUniteLegale,
                address: `${getEntreprise.adresseEtablissement.numeroVoieEtablissement ?? ""} ${getEntreprise.adresseEtablissement.typeVoieEtablissement} ${getEntreprise.adresseEtablissement.libelleVoieEtablissement} ${getEntreprise.adresseEtablissement.complementAdresseEtablissement ?? ""}`,
                postalCode: getEntreprise.adresseEtablissement.codePostalEtablissement,
                city: getEntreprise.adresseEtablissement.libelleCommuneEtablissement
            }

            const stepEntreprise = document.getElementById("wizardForm")
            const el = stepEntreprise.elements

            for (const element of el) {
                Object.keys(myEntreprise).find((item) => {
                    if (item === element.id) {
                        console.log(myEntreprise[item])
                        return element.value = myEntreprise[item]
                    }
                })
            }
        }
    },
    getEntrepriseBySiret: async function (siret) {
        if (siret.length < 14 || siret.length > 14 || isNaN(siret)) return siret = "error"

        return await fetch(`https://api.insee.fr/entreprises/sirene/V3/siret/${siret}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                Authorization: 'Bearer 085a2219-f3e3-32fb-b338-6ac748ccc98f'
            }
        })
            .then(resp => resp.json())
            .then(data => data.etablissement);
    },
    submitForm: function (target) {
        const form = document.forms[target.id]
        const selectSource = form.elements["selectSource"]
        const selectProperty = form.elements["selectProperty"]

        console.log(selectSource.value)
        console.log(selectProperty.value)
    },
    labelValue: function (target) {
        const label = [...document.querySelectorAll(".form-label-value")]

        label.forEach(item => {
            const labelFor = item.htmlFor

            if (labelFor == target.id) {
                const selectedOption = target.selectedOptions[0];
                const selectedText = selectedOption.textContent;

                item.innerHTML = selectedText
                return
            }
        })
    },
    inputFile: function (el, target) {
        const {
            name: name,
            size: size
        } = el.files[0]

        if (!el.files[0]) return;

        let cible = document.querySelector(`.${target}`)
        cible.parentElement.classList.add("active")

        cible.querySelector(".name").innerHTML = name
        cible.querySelector(".size").innerHTML = `${size} KB`
    },
    validateForm: function () {
        const forms = document.querySelectorAll('.needs-validation');

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {
                if (!form.checkValidity()) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    },
    dataTables: function (id) {
        return new DataTable(document.getElementById(id), {
            info: false
        });
    },
}

// const h1 = global.elementFactory('h1', 'tamer', 'blue')
// const a = global.elementFactory('a', 'tamzzaaer', 'yellow')
// const p = global.elementFactory('p', 'cqsc', 'red')

const lol = {
    url_summoner: `https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/megalomanskej`,
    url_img: `http://ddragon.leagueoflegends.com/cdn/13.1.1/img/profileicon/5271.png`,
    url_masteries: `https://raw.communitydragon.org/latest/game/assets/ux/mastery/mastery_icon_4.png`,
    init: function (target) {
        const value = document.getElementById(target).value
        document.getElementById("giphy").innerHTML = ""

        const params = new URLSearchParams({
            // api_key:"h25TyBG9DDZznRXdI6aKol8bI5xX85j1",
            api_key: "RGAPI-38c94494-cc44-4fd2-a3fc-4525fc7e031b",
            // q:value,
            // limit:100,
            // offset:0,
            // rating:"g",
            // lang:"en",
        });

        fetch(`${giphy.url}?${params}`)
            .then(resp => resp.json())
            .then(data => {
                const items = data.data
                items.map(item => {
                    let tplImg = `
                    <li class="col-sm-6 col-md-4 col-lg-3">
                        <picture>
                            <source srcset="${item.images.fixed_height.webp}" type="image/webp">
                            <img
                                src="${item.images.fixed_height.url}"
                                class="img-thumbnail-override"
                                alt="${item.title}"
                                width="320"
                                height="${item.images.fixed_height.height}" />
                        </picture>
                    </li>`

                    document.getElementById("giphy").innerHTML += tplImg
                })
            })
            .catch(error => console.error(error));
    }
}

//mesurer le temps d'exécution d'une fonction js
// document.querySelector("#csv-file").addEventListener("change", function () {
//     const start = performance.now();

//     xlsxToJson.init(this, 'output')
//     // votre code à mesurer ici

//     const end = performance.now();
//     console.log(`Temps d'exécution : ${end - start} milliseconds.`);
// });
