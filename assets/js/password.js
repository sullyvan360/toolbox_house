const range = document.querySelector("#range")
const strengthDiv = document.querySelector("#strength")
const passwordInput = document.querySelector("#password")
const strength = {
    1: {
        strength: "weak",
        color: "red",
        width: "33%"
    },
    2: {
        strength: "medium",
        color: "orange",
        width: "66%"
    },
    3: {
        strength: "strong",
        color: "green",
        width: "100%"
    }
}

const updatePassword = {
    getIndicator: function ({ password, value }) {
        const charCodes = Array.from(password).map(char => char.charCodeAt(0));

        if (charCodes.some(charCode => charCode >= 65 && charCode <= 90))
            value.upper = true;

        if (charCodes.some(charCode => charCode >= 48 && charCode <= 57))
            value.numbers = true;

        if (charCodes.some(charCode => charCode >= 97 && charCode <= 122))
            value.lower = true;

        return strength[Object.values(value).filter(Boolean).length] || "";
    },
    getStrength: function ({ password }) {
        return updatePassword.getIndicator({ password, value: { upper: false, numbers: false, lower: false } });
    },
    handleChange: function () {
        const password = passwordInput.value;
        const strengthText = updatePassword.getStrength({ password });

        range.style.cssText = '';

        if (strengthText) {
            strengthDiv.innerText = `${strengthText.strength} Password`;
            range.style.setProperty("--range-background", strengthText.color);
            range.style.setProperty("--range-width", strengthText.width);
        } else {
            strengthDiv.innerText = "";
        }
    },
    submitForm: function (el, event) {
        const strength = updatePassword.getStrength({ password });
        const form = document.forms[el.id]

        if (strength.strength !== "strong") {
            form["password"].classList.add("is-invalid")
            return event.preventDefault();
        }
    }
}
