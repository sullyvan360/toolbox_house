const directus = {
    url: 'http://45.147.96.165/items/gf_',
    init: async function (collection, params = "") {
        return await fetch(`${directus.url}${collection}?${params}`)
            .then(resp => resp.json())
            .then(res => res.data);
    }
}
