const giphy = {
    url: `https://api.giphy.com/v1/gifs/search`,
    init: function (target) {
        const value = document.getElementById(target).value
        document.getElementById("giphy").innerHTML = ""

        const params = new URLSearchParams({
            api_key: "h25TyBG9DDZznRXdI6aKol8bI5xX85j1",
            q: `Michael Scott ${value}`,
            limit: 100,
            offset: 0,
            rating: "g",
            lang: "en",
        });

        fetch(`${giphy.url}?${params}`)
            .then(resp => resp.json())
            .then(data => {
                const items = data.data
                items.map(item => {
                    let tplImg = `
                    <li class="col-sm-6 col-md-4 col-lg-3">
                        <picture>
                            <source srcset="${item.images.fixed_height.webp}" type="image/webp">
                            <img
                                src="${item.images.fixed_height.url}"
                                class="img-thumbnail-override"
                                alt="${item.title}"
                                width="320"
                                height="${item.images.fixed_height.height}"
                                onclick="giphy.copy(this)"/>
                        </picture>

                    </li>`

                    document.getElementById("giphy").innerHTML += tplImg
                })
            })
            .catch(error => console.error(error));
    },
    copy: function (target) {
        navigator.clipboard.writeText(target.src)
    },
}