const qr_code = {
    init: function (target) {
        const value = document.getElementById(target).value

        document.getElementById("qrcode").innerHTML = ""

        let create_qr_code = new QRCodeStyling({
            width: 300,
            height: 300,
            type: "svg",
            data: value,
            // image: "https://upload.wikimedia.org/wikipedia/commons/5/51/Facebook_f_logo_%282019%29.svg",
            dotsOptions: {
                color: "#4267b2",
                type: "rounded"
            },
            backgroundOptions: {
                color: "#e9ebee",
            },
            imageOptions: {
                crossOrigin: "anonymous",
                margin: 20
            }
        })

        create_qr_code.append(document.getElementById("qrcode"));
    }
}