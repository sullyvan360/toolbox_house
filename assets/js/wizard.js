let formStepsNum = 0;
const formSteps = document.querySelectorAll(".form-step")
const formStepActive = document.getElementsByClassName("form-step-active")[0]
const progress = document.getElementById("progress")
const progressSteps = document.querySelectorAll(".progress-step")
let oui = false;

const wizard = {
    init: function () {
        const button = [...document.getElementsByClassName("button-action")]

        button.map(item => {
            item.addEventListener("click", function () {

                if (this.classList.contains("next")) {
                    if (formStepsNum > (progressSteps.length - 2)) return false

                    formStepsNum++

                    if (formStepsNum > 0)
                        this.previousElementSibling.classList.remove("invisible")
                } else {
                    formStepsNum--
                    if (formStepsNum < 1)
                        this.classList.add("invisible")
                }

                wizard.updateFormSteps()
                wizard.updateProgressbar()
            })
        })
    },
    updateFormSteps: function () {
        formSteps.forEach((formStep) => {
            formStep.classList.contains("form-step-active") &&
                formStep.classList.remove("form-step-active")
        });

        formSteps[formStepsNum].classList.add("form-step-active")
    },
    updateProgressbar: function () {
        progressSteps.forEach((progressStep, idx) => {
            if (idx < formStepsNum + 1) {
                progressStep.classList.add("progress-step-active")
            } else {
                progressStep.classList.remove("progress-step-active")
            }
        });

        const progressActive = document.querySelectorAll(".progress-step-active")

        progress.style.width =
            ((progressActive.length - 1) / (progressSteps.length - 1)) * 100 + "%"
    },
}
